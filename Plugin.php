<?php

namespace Keios\Simplemenu;

use Backend;
use System\Classes\PluginBase;

/**
 * SimpleMenu Plugin Information File.
 */
class Plugin extends PluginBase
{
    /**
     * @var array
     */
    public $require = [
        'RainLab.Translate',
    ];

    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
        'name' => 'SimpleMenu Manager',
        'description' => 'Plugin for simple menu management',
        'author' => 'Jakub Zych',
        'icon' => 'icon-leaf',
        ];
    }

    /**
     * @return array
     */
    public function registerComponents()
    {
        return [
            'Keios\SimpleMenu\Components\MenuComponent' => 'menucomponent',
        ];
    }

    /**
     * @return array
     */
    public function registerNavigation()
    {
        return [
            'simplemenu' => [
                'label' => 'keios.simplemenu::lang.strings.nav-label',
                'url' => Backend::url('keios/simplemenu/menueditor'),
                'icon' => 'icon-list-ul',
                'order' => 500,
                'permissions' => ['keios.simplemenu.*'],

                'sideMenu' => [
                    'menugroup' => [
                        'label' => 'keios.simplemenu::lang.strings.menugroups',
                        'url' => Backend::url('keios/simplemenu/menugroup'),
                        'icon' => 'icon-pencil',
                        'permissions' => ['keios.simplemenu.*'],
                    ],
                    'menueditor' => [
                        'label' => 'keios.simplemenu::lang.strings.menueditor',
                        'icon' => 'icon-list-ul',
                        'url' => Backend::url('keios/simplemenu/menueditor'),
                        'permissions' => ['keios.simplemenu.*'],
                    ],
                    'childeditor' => [
                        'label' => 'keios.simplemenu::lang.strings.childeditor',
                        'icon' => 'icon-list-ul',
                        'url' => Backend::url('keios/simplemenu/childeditor'),
                        'permissions' => ['keios.simplemenu.*'],
                    ],
                ],
            ],
        ];
    }

    /**
     * @return array
     */
    public function registerSettings()
    {
        return [];
    }
}
