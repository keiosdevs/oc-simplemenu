<?php

namespace Keios\Simplemenu\Components;

use Cms\Classes\ComponentBase;
use Keios\SimpleMenu\Models\Menu;
use Keios\SimpleMenu\Models\MenuGroup;

/**
 * Class MenuComponent
 *
 * @package Keios\Simplemenu\Components
 */
class MenuComponent extends ComponentBase
{
    /**
     * @return array
     */
    public function componentDetails()
    {
        return [
            'name' => 'SimpleMenu Component',
            'description' => 'Displays menu elements.',
        ];
    }

    /**
     * @return array
     */
    public function defineProperties()
    {
        return [
            'menuGroup' => [
                'title' => 'keios.simplemenu::lang.strings.menugroup',
                'description' => 'Menu Group to display',
                'type' => 'dropdown',
                'default' => '',
            ],
        ];
    }

    /**
     * @return mixed
     */
    public function getmenuGroupOptions()
    {
        return MenuGroup::lists('name', 'id');
    }


    /**
     * @return mixed
     */
    public function simpleMenu()
    {
        $selectedMenuGroupId = $this->property('menuGroup');
        $cacheKey = 'simplemenu' . '_' . $selectedMenuGroupId;
        $simpleMenu = Menu::where('menu_group_id', $selectedMenuGroupId)->orderBy('position', 'asc')
            ->with([
                'children' => function ($subQuery) use ($cacheKey) { // syntax
                    $subQuery->remember(118000, $cacheKey .'_children');
                },
            ])
            ->remember(118000, $cacheKey .'_menu')
            ->get();
        return $simpleMenu;
    }
}
