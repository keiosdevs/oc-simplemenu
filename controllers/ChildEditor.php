<?php

namespace Keios\Simplemenu\Controllers;

use BackendMenu;
use Backend\Classes\Controller;
use Flash;
use Lang;
use Keios\Simplemenu\Models\Child as ChildModel;

/**
 * ChildEditor Back-end Controller.
 */
class ChildEditor extends Controller
{
    /**
     * @var array
     */
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController',
    ];

    /**
     * @var string
     */
    public $formConfig = 'config_form.yaml';
    /**
     * @var string
     */
    public $listConfig = 'config_list.yaml';

    /**
     * ChildEditor constructor.
     */
    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Keios.Simplemenu', 'simplemenu', 'childeditor');
    }

    /**
     * Deleted checked products.
     */
    public function index_onDelete()
    {
        if (($checkedIds = post('checked')) && is_array($checkedIds) && count($checkedIds)) {

            foreach ($checkedIds as $productId) {
                if (!$product = ChildModel::find($productId)) {
                    continue;
                }
                $product->delete();
            }

            Flash::success(Lang::get('keios.simplemenu::lang.strings.delete_selected_success'));
        } else {
            Flash::error(Lang::get('keios.simplemenu::lang.strings.delete_selected_empty'));
        }

        return $this->listRefresh();
    }
}
