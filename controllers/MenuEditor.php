<?php

namespace Keios\Simplemenu\Controllers;

use BackendMenu;
use Backend\Classes\Controller;
use Flash;
use Lang;
use Keios\Simplemenu\Models\Menu as MenuModel;

/**
 * Menu Editor Back-end Controller.
 */
class MenuEditor extends Controller
{
    /**
     * @var array
     */
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController',
    ];

    /**
     * @var string
     */
    public $formConfig = 'config_form.yaml';
    /**
     * @var string
     */
    public $listConfig = 'config_list.yaml';

    /**
     * MenuEditor constructor.
     */
    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Keios.Simplemenu', 'simplemenu', 'menueditor');
    }

    /**
     * @var
     */
    protected $menuGroupId;

    /**
     * @param null $menuGroupId
     */
    public function index($menuGroupId = null)
    {
        // Store the routed parameter to use later
        $this->menuGroupId = $menuGroupId;

        // Call the ListController behavior standard functionality
        $this->asExtension('ListController')->index();
    }

    /**
     * @param $query
     */
    public function listExtendQuery($query)
    {
        // Extend the list query to filter by the user id
        if ($this->menuGroupId) {
            $query->where('menu_group_id', $this->menuGroupId);
        }
    }

    /**
     * Deleted checked products.
     */
    public function index_onDelete()
    {
        if (($checkedIds = post('checked')) && is_array($checkedIds) && count($checkedIds)) {

            foreach ($checkedIds as $productId) {
                if (!$product = MenuModel::find($productId)) {
                    continue;
                }
                $product->delete();
            }

            Flash::success(Lang::get('keios.simplemenu::lang.strings.delete_selected_success'));
        } else {
            Flash::error(Lang::get('keios.simplemenu::lang.strings.delete_selected_empty'));
        }

        return $this->listRefresh();
    }
}
