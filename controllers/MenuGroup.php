<?php

namespace Keios\Simplemenu\Controllers;

use BackendMenu;
use Backend\Classes\Controller;
use Flash;
use Lang;
use Keios\Simplemenu\Models\MenuGroup as MenuGroupModel;

/**
 * Menu Group Back-end Controller.
 */
class MenuGroup extends Controller
{
    /**
     * @var array
     */
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController',
        'Backend.Behaviors.RelationController',
    ];

    /**
     * @var string
     */
    public $formConfig = 'config_form.yaml';
    /**
     * @var string
     */
    public $listConfig = 'config_list.yaml';
    /**
     * @var string
     */
    public $relationConfig = 'config_relation.yaml';

    /**
     * MenuGroup constructor.
     */
    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Keios.Simplemenu', 'simplemenu', 'menugroup');
    }

    /**
     * Deleted checked products.
     */
    public function index_onDelete()
    {
        if (($checkedIds = post('checked')) && is_array($checkedIds) && count($checkedIds)) {

            foreach ($checkedIds as $productId) {
                if (!$product = MenuGroupModel::find($productId)) continue;
                $product->delete();
            }

            Flash::success(Lang::get('keios.simplemenu::lang.strings.delete_selected_success'));
        }
        else {
            Flash::error(Lang::get('keios.simplemenu::lang.strings.delete_selected_empty'));
        }

        return $this->listRefresh();
    }
}
