<?php

namespace Keios\Simplemenu\Models;

use Model;
use Cache;

/**
 * Child Model.
 */
class Child extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'keios_simplemenu_children';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = ['title', 'url', 'position', 'target_blank', 'icon_class', 'menu'];

    /**
     * @var array Relations
     */
    public $belongsTo = [
        'menu' => ['Keios\Simplemenu\Models\Menu', 'table' => 'keios_simplemenu_menus', 'order' => 'title'],
    ];

    /**
     * @return mixed
     */
    public function getMenuOptions()
    {
        $allMenus = Menu::where('is_parent', '=', 1)->lists('title');

        return $allMenus;
    }

    /**
     * @param null $keyValue
     * @param null $fieldName
     *
     * @return array
     */
    public function listParents($keyValue = null, $fieldName = null)
    {
        $menus = Menu::all()->getDictionary();
        $titles = [];
        $ids = [];
        foreach ($menus as $menu) {
            if ($menu['is_parent'] == 1) {
                array_push($ids, $menu['id']);
                array_push($titles, $menu['title']);
            }
        }
        $result = array_combine($ids, $titles);

        return $result;
    }

    /**
     *
     */
    public function afterSave()
    {
        Cache::forget('simplemenu.children');
    }

    /**
     *
     */
    public function afterDelete()
    {
        Cache::forget('simplemenu.children');
    }
}
