<?php

namespace Keios\Simplemenu\Models;

use Model;
use Cache;

/**
 * menu Model.
 */
class Menu extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'keios_simplemenu_menus';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = ['title', 'url', 'position', 'target_blank', 'icon_class', 'is_parent', 'menu_group_id'];

    /**
     * @var array Relations
     */
    public $belongsTo = [
        'menu_group' => [
            'Keios\Simplemenu\Models\MenuGroup',
            'table' => 'keios_simplemenu_menu_groups',
            'order' => 'name',
        ],
    ];

    /**
     * @var array
     */
    public $hasMany = [
        'children' => ['Keios\Simplemenu\Models\Child'],
    ];

    /**
     * @return mixed
     */
    public function getMenuGroupOptions()
    {
        $allMenus = MenuGroup::all()->lists('name', 'id');

        return $allMenus;
    }

    /**
     * @param null $keyValue
     * @param null $fieldName
     *
     * @return array
     */
    public function listGroups($keyValue = null, $fieldName = null)
    {
        $menugroups = MenuGroup::all()->getDictionary();
        $names = [];
        $ids = [];
        foreach ($menugroups as $menu) {
            array_push($ids, $menu['id']);
            array_push($names, $menu['name']);
        }

        $result = array_combine($ids, $names);

        return $result;
    }

    /**
     *
     */
    public function afterSave()
    {
        Cache::forget('simplemenu.menu');
    }

    /**
     *
     */
    public function afterDelete()
    {
        Cache::forget('simplemenu.menu');
    }
}
