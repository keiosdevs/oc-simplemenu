<?php

namespace Keios\Simplemenu\Models;

use Model;

/**
 * MenuGroup Model.
 */
class MenuGroup extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'keios_simplemenu_menu_groups';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = ['name'];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    /**
     * @var array
     */
    public $hasMany = [
        'menus' => ['Keios\Simplemenu\Models\Menu'],
    ];
    /**
     * @var array
     */
    public $belongsTo = [];
    /**
     * @var array
     */
    public $belongsToMany = [];
    /**
     * @var array
     */
    public $morphTo = [];
    /**
     * @var array
     */
    public $morphOne = [];
    /**
     * @var array
     */
    public $morphMany = [];
    /**
     * @var array
     */
    public $attachOne = [];
    /**
     * @var array
     */
    public $attachMany = [];
}
