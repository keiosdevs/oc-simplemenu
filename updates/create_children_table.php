<?php

namespace Keios\Simplemenu\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

/**
 * Class create_children_table
 *
 * @package Keios\Simplemenu\Updates
 */
class create_children_table extends Migration
{
    public function up()
    {
        Schema::create('keios_simplemenu_children', function ($table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('title');
            $table->string('url');
            $table->integer('position');
            $table->string('icon_class')->nullable();
            $table->boolean('target_blank');
            $table->integer('menu_id')->unsigned()->index();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('keios_simplemenu_children');
    }
}
