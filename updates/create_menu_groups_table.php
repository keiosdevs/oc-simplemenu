<?php

namespace Keios\Simplemenu\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

/**
 * Class CreateMenuGroupsTable
 *
 * @package Keios\Simplemenu\Updates
 */
class CreateMenuGroupsTable extends Migration
{
    public function up()
    {
        Schema::create('keios_simplemenu_menu_groups', function ($table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('keios_simplemenu_menu_groups');
    }
}
