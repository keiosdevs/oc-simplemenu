<?php

namespace Keios\Simplemenu\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;
use DB;

/**
 * Class create_menus_table
 *
 * @package Keios\Simplemenu\Updates
 */
class create_menus_table extends Migration
{
    public function up()
    {
        Schema::create('keios_simplemenu_menus', function ($table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('title');
            $table->string('url');
            $table->integer('position');
            $table->string('icon_class')->nullable();
            $table->boolean('target_blank');
            $table->boolean('is_parent');
            $table->integer('menu_group_id')->default(1)->unsigned()->index();
            $table->timestamps();
        });
        DB::update('ALTER TABLE keios_simplemenu_menus AUTO_INCREMENT = 1;');
    }

    public function down()
    {
        Schema::dropIfExists('keios_simplemenu_menus');
    }
}
