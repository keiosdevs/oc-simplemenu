<?php

namespace Keios\Simplemenu\Updates;

use Keios\Simplemenu\Models\MenuGroup;
use Seeder;
use Keios\Simplemenu\Models\Menu;

/**
 * Class SeedAllTables
 *
 * @package Keios\Simplemenu\Updates
 */
class SeedAllTables extends Seeder
{
    public function run()
    {
        MenuGroup::insert([
            [ 'name' => 'Main Menu' ],
        ]);

        Menu::insert([
            [
                'title'         => 'About us',
                'url'           => '/about',
                'position'      => '10',
                'icon_class'    => 'icon-print',
                'target_blank'  => false,
                'is_parent'     => false,
                'menu_group_id' => 1,
            ],
            [
                'title'         => 'Promotions',
                'url'           => '/deals',
                'position'      => '20',
                'icon_class'    => 'icon-new',
                'target_blank'  => false,
                'is_parent'     => false,
                'menu_group_id' => 1,
            ],
            [
                'title'         => 'Portfolio',
                'url'           => '/portfolio',
                'position'      => '30',
                'icon_class'    => 'icon-folder-images',
                'target_blank'  => false,
                'is_parent'     => false,
                'menu_group_id' => 1,
            ],
            [
                'title'         => 'Create order',
                'url'           => '/',
                'position'      => '40',
                'icon_class'    => 'icon-palette',
                'target_blank'  => false,
                'is_parent'     => false,
                'menu_group_id' => 1,
            ],
            [
                'title'         => 'Terms of use',
                'url'           => '/terms',
                'position'      => '50',
                'icon_class'    => 'icon-open-book',
                'target_blank'  => false,
                'is_parent'     => false,
                'menu_group_id' => 1,
            ],
            [
                'title'         => 'Contact',
                'url'           => '/contact',
                'position'      => '60',
                'icon_class'    => 'icon-mail',
                'target_blank'  => false,
                'is_parent'     => false,
                'menu_group_id' => 1,
            ],
        ]);
    }
}
